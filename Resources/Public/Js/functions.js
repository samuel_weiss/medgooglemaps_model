(function($) {
    $(document).ready(function() {    
        $(document).on("click", ".medgooglemaps_navigation_menue a", function(e) {
            e.preventDefault();  

            // Add font-weight
            $(this).css("fontWeight","bold");
            $(this).siblings().css("fontWeight","normal");
        
            // Change input field
            if($(".medgooglemaps_input_1").attr("name") == 'saddr') {
                $(".medgooglemaps_input_1").attr("name","daddr");
            }   
            else if($(".medgooglemaps_input_1").attr("name") == 'daddr') {
                $(".medgooglemaps_input_1").attr("name","saddr");
            }  
            
            if($(".medgooglemaps_input_2").attr("name") == 'saddr') {
                $(".medgooglemaps_input_2").attr("name","daddr");
            }   
            else if($(".medgooglemaps_input_2").attr("name") == 'daddr') {
                $(".medgooglemaps_input_2").attr("name","saddr");
            }          
        });

        $(document).on("click", "a.medgooglemaps_submit", function() {
            $(this).parents("form").submit();
        });
    });
})(jQuery);