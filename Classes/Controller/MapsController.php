<?php
namespace MED\Medgooglemaps\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use RZ\Medgooglemaps\Utility\T3jquery;

/**
 * MapsController
 */
class MapsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * mapsRepository
	 *
	 * @var \MED\Medgooglemaps\Domain\Repository\MapsRepository
	 * @inject
	 */
	protected $mapsRepository = NULL;

	/**
	 * markerRepository
	 *
	 * @var \MED\Medgooglemaps\Domain\Repository\MarkerRepository
	 * @inject
	 */
	protected $markerRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction( ) {
		// t3jquery
		$t3jqueryCheck = T3jquery::check( );

		// Add jQuery?
		if ($t3jqueryCheck === false) {
			if ($this->settings['addJquery']) {
				if ($this->settings['addToFooter']) {
					$GLOBALS['TSFE']->additionalFooterData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.3.min.js"></script>';
				} else {
					$GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.3.min.js"></script>';
				}
			}
		}
		// Add JS files
		$GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_api'] = '<script type="text/javascript" src="' . $this->settings['apiUrl'] . '"></script>';

		if( $this->settings['addToFooter'] ) {
			$GLOBALS['TSFE']->additionalFooterData['medgooglemaps_js'] = '<script type="text/javascript" src="' . $this->settings['jsFile'] . '"></script>';
		} else {
			$GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_js'] = '<script type="text/javascript" src="' . $this->settings['jsFile'] . '"></script>';
		}

		$map = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( '\MED\Medgooglemaps\Domain\Model\Maps' );

		$map->setControls( $this->settings['controls'] );

		// Marker new
		$cObj = $this->configurationManager->getContentObject( );
		$uid = $cObj->data['uid'];
		$map->setUid( $uid );

		$markers = explode( ",", $this->settings['marker'] );
		$markers = $this->markerRepository->setRespectStoragePage( false )->findByFilter( array( array( array(
					'uid',
					'in',
					$markers
				) ) ) );

		$map->setMarkers( $markers );
		$map->setLatitude( $this->settings['latitude'] );
		$map->setSettings( $this->settings );
		$map->render( );

		// Template vars
		$this->view->assign( 'settings', $this->settings );
		$this->view->assign( 'uid', $uid );
	}

	protected function getPartialView( $templateName, array $variables = array(), $prefix = '' ) {
		$partialView = new \TYPO3\CMS\Fluid\View\StandaloneView( );
		$partialView->setFormat( 'html' );
		$extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration( \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK );
		$templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName( $extbaseFrameworkConfiguration['view']['partialRootPath'] );
		$templatePathAndFilename = $templateRootPath . $prefix . $templateName . '.html';
		$partialView->setTemplatePathAndFilename( $templatePathAndFilename );
		$partialView->assignMultiple( $variables );
		$extensionName = $this->request->getControllerExtensionName( );
		$partialView->getRequest( )->setControllerExtensionName( $extensionName );

		return $partialView;
	}

	/**
	 * Will process the input string with the parseFunc function from tslib_cObj
	 * based on configuration set in "lib.parseFunc_RTE" in the current TypoScript
	 * template.
	 * This is useful for rendering of content in RTE fields where the transformation
	 * mode is set to "ts_css" or so.
	 * Notice that this requires the use of "css_styled_content" to work right.
	 *
	 * @param   string      The input text string to process
	 * @return  string      The processed string
	 * @see tslib_cObj::parseFunc()
	 */
	public function pi_RTEcssText( $str, $cObj ) {
		$parseFunc = $GLOBALS['TSFE']->tmpl->setup['lib.']['parseFunc_RTE.'];
		if( is_array( $parseFunc ) )
			$str = $cObj->parseFunc( $str, $parseFunc );
		return $str;
	}

	protected function debug( $var ) {
		print_r( "<pre>" ) . print_r( $var ) . print_r( "</pre>" );
	}

}
