<?php
namespace MED\Medgooglemaps\Domain\Model;
/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
/**
 * Google Maps
 */
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
class Maps extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	/**
	 * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
	 */
	protected $cObj;

	protected $controls;
	public function setControls( $controls ) {
		$this->controls = $controls;
	}

	protected $markers;
	public function setMarkers( $markers ) {
		$this->markers = $markers;
	}

	protected $settings;
	public function setSettings( $settings ) {
		$this->settings = $settings;
	}

	protected $latitude;
	public function setLatitude( $latitude ) {
		$this->latitude = $latitude;
	}

	protected $uid;
	public function setUid( $uid ) {
		$this->uid = $uid;
	}

	public function render( ) {
		if( $this->controls ) {
			$controlsArr = explode( ",", $this->controls );
			$controlsConfig = ',';
			foreach( $controlsArr as $c ) {
				if( $c == 'scaleControl' ) {
					$controlsConfig .= '
                    ' . $c . ': true,
                ';
				} else {
					$controlsConfig .= '
                    ' . $c . ': false,
                ';
				}
			}
			$controlsConfig = substr( $controlsConfig, 0, - 1 );
		}
		/*
		 JS
		 */
		// Lat + Lng
		$latitude = $this->latitude;
		$longitude = $this->settings['longitude'];
		if( $latitude ) {
			$latitude = $latitude;
		} else {
			$latitude = 0;
		}
		if( $longitude ) {
			$longitude = $longitude;
		} else {
			$longitude = 0;
		}
		
		// Zoom
		$zoom = $this->settings['zoom'];
		// Style
		$style = $this->settings['style'];
		if( $style ) {
			$theme = $this->getPartialView( 'Theme-' . $this->settings['style'], array( ), 'Themes/' );
			$styleOutput = 'styles: ' . $theme->render( ) . ',';
		}
		// Map type
		$mapType = $this->settings['mapType'];
		if( $mapType ) {
			$mapTypeOutput = $mapType;
		} else {
			$mapTypeOutput = 'MapTypeId.ROADMAP';
		}
		// Markers
		if( $this->markers ) {
			$i = 1;
			$this->cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( '\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer' );
			$parseFunc = $GLOBALS['TSFE']->tmpl->setup['lib.']['parseFunc_RTE.'];
			$infotext = '';
			foreach( $this->markers as $marker ) {
				// Clear vars
				$infoTextOutput = '';
				$navigationOutput = '';
				if( $marker->getInfotext( ) ) {
					if( is_array( $parseFunc ) ) {
						$infotext = $this->cObj->parseFunc( $marker->getInfotext( ), $parseFunc );
					}
					//$infotext = \TYPO3\CMS\Frontend\Plugin::pi_RTEcssText($marker->getInfotext(),
					// $cObj);
					$infotext = str_replace( array(
						"\r",
						"\n"
					), "", $infotext );
					$infoTextOutput = '\'<div class="medgooglemaps_content">' . $infotext . '</div>\'';
					if( $marker->getNavigation( ) ) {
						$infoTextOutput .= ',';
						// Config for fluid standalone view for markers
						$confArr = array(
							'settings' => array( 'googleMapsSubmitClasses' => $this->settings['googleMapsSubmitClasses'] ),
							'marker' => $marker
						);
						$navigation = $this->getPartialView( 'Navigation', $confArr );
						$navigationOutput = json_encode( $navigation->render( ) );
					}
				}
				$markerOutput .= "
                var contentString" . $i . " = 
                [" . $infoTextOutput . "
                " . $navigationOutput . "
                ].join('\\n');
            ";
				// Locations
				if( $marker->getInfowindowautoopen( ) )
					$infoWindowAutoOpen = $marker->getInfowindowautoopen( );
				else
					$infoWindowAutoOpen = 0;
				if( $this->settings['customMarkerPath'] )
					$customMarkerPath = $this->settings['customMarkerPath'];
				else
					$customMarkerPath = 'uploads/pics/';
				if( $marker->getMarkercustom( ) )
					$markerCustom = $customMarkerPath . $marker->getMarkercustom( );
				$locationsOutput .= "
                    [contentString" . $i . ", " . $marker->getLatitude( ) . ", " . $marker->getLongitude( ) . ", " . $infoWindowAutoOpen . ", '" . $marker->getMarkercolor( ) . "', '" . $markerCustom . "']
            ";
				if( $i < sizeof( $this->markers ) )
					$locationsOutput .= ",";
				$i++;
			}
			$locationsOutput = "
            var locations = [
                " . $locationsOutput . "
            ];
        ";
			$autocalcmapcenter = $this->settings['autocalcmapcenter'];
			$processMarker = '
            var i;

            for (i = 0; i < locations.length; i++) {';
			// Wenn autocalcmapcenter gesetzt sind, Umrandung der Marker erweitern
			if( $autocalcmapcenter ) {
				$processMarker .= 'bounds.extend(new google.maps.LatLng(locations[i][1], locations[i][2]));';
			}
			$processMarker .= 'if(locations[i][4]) {
                    var markerIcon = "' . ExtensionManagementUtility::siteRelPath( 'medgooglemaps' ) . 'Resources/Public/Icons/marker2/"+locations[i][4]+".png";
                }
                else {
                    var markerIcon = "";
                }

                if(locations[i][5]) {
                    var markerIcon = locations[i][5];   
                }

                marker[i] = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map' . $this->uid . ',
                    icon: markerIcon
                });
                
                if(locations[i][0]) {
                    marker[i].medInfoWindow = new google.maps.InfoWindow({
                        content: locations[i][0],
                        disableAutoPan: true
                    });

                    google.maps.event.addListener(marker[i], "click", function() {
                        // Close previously opened infowindows
                        for (i = 0; i < locations.length; i++) { 
                            marker[i].medInfoWindow.close();
                        }

                        this.medInfoWindow.disableAutoPan = false;
                        this.medInfoWindow.open(map' . $this->uid . ',this);
                    }); 
  
                    if(locations[i][3] == 1) {
                        autoOpen.push(i);
                    }
                }
            }
        ';
		}
		if( intval( $this->settings['deactivateScrollWheel'] ) != 0 )
			$scrollWheel = 'false';
		else
			$scrollWheel = 'true';
		$js = '<script type="text/javascript">';
		$js .= '
        var map' . $this->uid . ';
        var marker = [];
        var autoOpen = [];

        function initialize() {
            var myLatlng' . $this->uid . ' = new google.maps.LatLng(' . $latitude . ',' . $longitude . ');

            var myOptions' . $this->uid . ' = {
            	scrollwheel: ' . $scrollWheel . ',
                zoom: ' . $zoom . ',
                ' . $styleOutput . '
                center: myLatlng' . $this->uid . ',
                mapTypeId: google.maps.' . $mapTypeOutput . '
                ' . $controlsConfig . '
            }
            map' . $this->uid . ' = new google.maps.Map(document.getElementById("map_canvas_' . $this->uid . '"), myOptions' . $this->uid . ');';
		if( $autocalcmapcenter )
			$js .= 'var bounds = new google.maps.LatLngBounds();';
		$js .= $markerOutput . '
            ' . $locationsOutput . '
            ' . $processMarker;
		if( $autocalcmapcenter ) {
			$js .= 'map' . $this->uid . '.fitBounds(bounds);';
			$js .= 'map' . $this->uid . '.panToBounds(bounds);';
			$js .= 'var boundsChangedListener = google.maps.event.addListenerOnce(map' . $this->uid . ', \'bounds_changed\', function(event) {
					if(this.getZoom())
						this.setZoom(myOptions' . $this->uid . '.zoom);
					google.maps.event.removeListener(boundsChangedListener);
				});';
		}
		$js .= 'google.maps.event.addListenerOnce(map' . $this->uid . ', "idle", function() {
                if(autoOpen.length > 0) {
                    for (var i = 0; i < autoOpen.length; i++) {
                        marker[autoOpen[i]].medInfoWindow.open(map' . $this->uid . ', marker[autoOpen[i]]);
                    }
                }
            });
        }

        google.maps.event.addDomListener(window, "load", initialize); 
    ';
		$js .= '</script>';
		$GLOBALS['TSFE']->additionalFooterData['medgooglemaps'] .= $js;
		return $content;
	}

}
